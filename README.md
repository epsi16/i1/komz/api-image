# API Image

## Installation
- `cp docker-compose.yml.dist docker-compose.yml`
    - Ajouter vos `SCALEWAY_ACCESS_KEY`, `SCALEWAY_SECRET_KEY`, `SCALEWAY_REGION`, `SCALEWAY_BUCKET_ID` et `SCALEWAY_BUCKET_ENDPOINT`
- `cp .env.dist .env`
    - Ajouter vos `SCALEWAY_ACCESS_KEY`, `SCALEWAY_SECRET_KEY`, `SCALEWAY_REGION`, `SCALEWAY_BUCKET_ID` et `SCALEWAY_BUCKET_ENDPOINT`
- `make install`

## Démarrage
- `make start`

## Arrêt
- `make stop`

## Supprimer le conteneur et ses images
- `make clean`

## Consulter les logs
- `make logs`

## Réccupérer l'IP du conteneur
- `make ip`
