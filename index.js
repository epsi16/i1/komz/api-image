const express = require('express');
const app = express();
const cors = require('cors');
const AWS = require("aws-sdk");
const multer = require('multer');
const multerS3 = require('multer-s3');
const config = require('./config');

app.use(cors());

const s3 = new AWS.S3(
    new AWS.Config({
        accessKeyId: config.SCALEWAY_ACCESS_KEY,
        secretAccessKey: config.SCALEWAY_SECRET_KEY,
        region: config.SCALEWAY_REGION,
        s3BucketEndpoint: true,
        endpoint: config.SCALEWAY_BUCKET_ENDPOINT
    })
);

var upload = multer({
    storage: multerS3({
        s3: s3,
        bucket: config.SCALEWAY_BUCKET_ID,
        acl: 'public-read',
        key: function (req, file, cb) {
            cb(null, req.body.name + '.' + file.originalname.split('.')[1]);
        }
    })
});

app.post('/image', upload.single('content'), (req, res) => {

    console.log("[POST] /image");

    res.header('Access-Control-Allow-Origin', '*');

    res.send({
        'url': req.file.location,
    });
});

app.listen(8080, () => {
    console.log("Server listening on port 8080...");
})
